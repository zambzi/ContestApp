define([
    "ember",
    "text!../templates/"+require("config").templateName+"/ranking.html", ],
    function( ember, landingTemplate ){
	
	var applicationView = ember.View.extend({
		defaultTemplate : ember.Handlebars.compile(landingTemplate),
	})
	return applicationView;
});