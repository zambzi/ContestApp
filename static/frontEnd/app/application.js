define([ "views/LandingView",
         "views/LoginView",
         "views/GameView",
         "views/RankingView",
         "controllers/ApplicationController", 
         "router", "bridge" ], function( LandingView, LoginView, 
        		 GameView, RankingView, ApplicationController, router, bridge ){
	
	var App = {
			LandingView : LandingView,
			GameView : GameView,
			RankingView : RankingView,
			LoginView : LoginView,
			ApplicationController : ApplicationController,
			Router : router
	};
	App.Router.map( function(){
		this.resource( 'landing', { path: '/' } );
		this.resource( 'login', { path: '/login' } );
		this.resource( 'game', { path: '/game' } );
		this.resource( 'results', { path: '/results' } );
	} );

	return App;
});