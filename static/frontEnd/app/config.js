define({
	app_name: "contestApp",
	templateName: document.getElementsByName("template_name")[0].getAttribute("content"),
	clientKey: document.getElementsByName("client_key")[0].getAttribute("content"), 
	shim: {
		'ember' : {
			deps: ['handlebars', 'jquery'],
			exports: 'Ember',
		},
		'gracefulWebSocket' :{
			deps: ['jquery'],
		}
	},
	paths: {
		'App': 'application',
		'models' : 'models',
		'views' : 'views',
		'templates' : 'templates',
		'controllers' : 'controllers',
		
		'bridge': 'bridge/bridge',
			
		'jquery': '../lib/jquery.min',
		'gracefulWebSocket': '../lib/jquery.gracefulWebSocket',
		'handlebars': '../lib/handlebars.min',
		'ember': '../lib/ember',
		'emberData' : '../lib/ember-data',
			
		'text': '../lib/text',
		'hbs': '../lib/hbs',
		'domReady': '../lib/domReady',
	},
	hbs : {
		disableI18n: true,
		templateExtension: "html",
	},
})