/**
 * Bridge module.
 * Used for websockets communication
 * 
 */

define([ "gracefulWebSocket" ], function( gracefulWebSocket ){
	var bridge = {
		ws : $.gracefulWebSocket("ws://127.0.0.1:8080/ws"),
		
		/**
		 * Sends message to server via websockets
		 * @param type : int, defines type of the message
		 * @param data : object, additional data required by given message type
		 * @param key : String, current client key
		 */
		send : function( type, data, key ){
			var message = 	"{'type':"	+ type +
							",'data':"	+ JSON.stringify(data) +
							",'key':" 	+ key +
							"}";
			console.log( message );
			this.ws.send( message );
		},
		
		onMessage : function( event, callback ){
			console.log( event.data );
		}
	}
	
	bridge.ws.onmessage = bridge.onMessage;
	
	return bridge;
});