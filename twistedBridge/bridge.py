'''
Created on Oct 9, 2014

@author: Lukasz Piotrowski

Twisted Bridge
Core for Twisted based bridge for
websockets communication

'''
from main.communications.commHub import CommHub

from twisted.protocols import basic
from twisted.web.websockets import WebSocketsResource, WebSocketsProtocol, lookupProtocolForFactory
from django.conf import settings
from random import randint

class Bridge(basic.LineReceiver):
    commHub = None
    
    def __init__(self):
        self.commHub = CommHub()
    
    def connectionMade(self):
#         self.transport.write( "Established connection" )
        self.factory.clients.append(self)
 
    def connectionLost(self, reason):
        self.factory.clients.remove(self)
 
    def dataReceived(self, data):
#         import ipdb; ipdb.set_trace()
        self.message(self.commHub.processMessage(data, self.transport.getPeer()) ) 
 
    def message(self, message):
        self.transport.write( str(message) )
        
from twisted.internet.protocol import Factory

class BridgeFactory(Factory):
    protocol = Bridge
    clients = []
