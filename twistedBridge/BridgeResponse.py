'''
Created on Oct 14, 2014

@author: Lukasz Piotrowski

Error codes to be sent back to client

'''
import json

#DB:

GenericException = 1000

ClientNotFound = 1001
PlayerNotFound = 1002
GameSessionNotFound = 1003
GameStepNotFound = 1004

CannotCreateGameStep = 1011
CannotCreatePlayer = 1012
CannotCreateGameSession = 1013
NoSessionInProgress = 1014

#Message

MessageParsingError = 2000
WrongMessageStructure = 2001

NoTypeInMessage = 2002
NoDataInMessage = 2003
NoKeyInMessage = 2004

WrongTypeInMessage = 2012
WrongDataInMessage = 2013
WrongKeyInMessage = 2014
    
def getError( code, data = "" ):
    return json.dumps({"status":-1, "code":code, "data":data})

def getSuccess( data = None ):
    if data == None:
        return json.dumps({ "status":0 })
    else:
        return json.dumps({ "status":0, "data": data })