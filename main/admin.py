from django.contrib import admin
from main.models import ClientModel, PlayerModel, GameSessionModel,\
    GameStepModel, GameModel, TemplateModel, PointCalculationRulesModel
# Register your models here.

admin.site.register(ClientModel.Client)
admin.site.register(PlayerModel.Player)
admin.site.register(GameSessionModel.GameSession)
admin.site.register(GameStepModel.GameStep)
admin.site.register(GameModel.Game)
admin.site.register(TemplateModel.Template)
admin.site.register(PointCalculationRulesModel.PointCalculationRules)