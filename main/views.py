# from django.shortcuts import render
# from django.http import HttpResponse

# Create your views here.

from django.views import generic
from django.contrib.sites.shortcuts import get_current_site 
from main.models.ClientModel import Client
from django.shortcuts import get_object_or_404

import logging

logger = logging.getLogger(__name__)

logger.debug("dsadasda");

class MainPageView(generic.TemplateView):
    template_name = 'index.html'
    domain_name = ''
        
    def get(self, request, *args, **kwargs):
        self.setDomainName( request )
        return generic.TemplateView.get(self, request, *args, **kwargs)
    
    ''' sets domain_name. Removes port number, "http://", "www." '''
    def setDomainName( self, request ):
        self.domain_name = get_current_site(request).domain.__str__()
        self.domain_name = self.domain_name \
            .replace('http://', '') \
            .replace('www.', '') \
            [:self.domain_name.index(":")] 
            
    def getClient( self ):
        #client = Client.objects.get( domain=self.domain_name )
        client = get_object_or_404(Client, domain=self.domain_name )
        return client
    
    def getListOfAuxCssFiles( self ):
        files = self.getClient().game.cssDependencies
        fileList = files.split(';')
        return fileList
        
    def get_context_data(self, **kwargs):
        return { 
            'domain_name': self.domain_name,
            'spa_template_name': self.getClient().template.templateName,
            'client_key': self.getClient().clientKey,
            'client_regex': self.getClient().clientRegex,
            'game_name': self.getClient().game.gameName,
            'aux_css_files': self.getListOfAuxCssFiles(),
            'game_custom_settings': self.getClient().gameAdditionalSettings,
            'client_title': self.getClient().clientTitle,
            'rules_link' : self.getClient().rulesLink,
        }

main_page = MainPageView.as_view()