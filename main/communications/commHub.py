'''
Created on Oct 10, 2014

@author: Lukasz Piotrowski 

Communications Hub

Core module for receiving/sending messages from
Twisted Bridge, and handling actions on given messages.

recieved messages shoud look like:

{"type" : "#", "data" : "DATA", "key" : "KEY"}

where # is unique action type number, DATA is data structure containing
appropriate data to be used with selected action, KEY is unique client key

'''

import json
import ast
from main.helpers import PlayerHelper, RankingHelper
from twistedBridge import BridgeResponse
from main.models.GameSessionModel import GameSession

class CommHub:
    START_GAME = 2
    FINISH_GAME = 3
    GAME_STEP = 4
    GET_RANKINGS = 5
    
    currentGameSession = None
    
    def processMessage(self, message, peer):
        try:
            sentData = ast.literal_eval( message )
        except ValueError as err:
            print err.message
            return BridgeResponse.getError( BridgeResponse.MessageParsingError )
        return self.__executeAction( sentData, peer )
        
#     def createPlayerAction(self, data, peer):
#         if "name" in data and "key" in data:
#             return PlayerHelper.createPlayer(data["name"], data["key"], peer)
#         else:
#             return BridgeResponse.getError( BridgeResponse.WrongDataInMessage )
        
    def startGameAction(self, data, key, peer):
        if "name" in data:
            return PlayerHelper.startGame( data["name"], key, peer )  
        else:
            return BridgeResponse.getError( BridgeResponse.WrongDataInMessage )
    
    def finishGameAction(self, data):
        return "finish"
        
    def gameStepAction(self, data, key):
#         import ipdb; ipdb.set_trace()
        if "key" in data and "value" in data:
            return PlayerHelper.createGameStep(data, key, self.currentGameSession)  
        else:
            return BridgeResponse.getError( BridgeResponse.WrongDataInMessage )
        
    def getRankingsAction(self, key):
        rankings = RankingHelper.getRanking(key, False)
        return BridgeResponse.getSuccess( rankings )
        
    def gameFinishAction(self, data, key):
        self.gameStepAction(data, key)
        #here you can put any validation code for winner
        
    def __setCurrentGameSession( self, session ):
        self.currentGameSession = session
        
        
    def __executeAction(self, sentData, peer):
        reply = BridgeResponse.getSuccess()
        if "type" in sentData and "data" in sentData and "key" in sentData:
            messageId = int(sentData.get("type"))
            messageData = sentData.get("data")
            messageKey = sentData.get("key")
            
            if messageId == self.FINISH_GAME:
                reply = self.finishGameAction( messageData, messageKey )
            elif messageId == self.START_GAME:
                startGameData = self.startGameAction( messageData, messageKey, peer )
                reply = startGameData[0]
                self.__setCurrentGameSession( startGameData[1] )
            elif messageId == self.GAME_STEP:
                reply = self.gameStepAction( messageData, messageKey )
            elif messageId == self.GET_RANKINGS:
                reply = self.getRankingsAction( messageKey )
            else:      
                reply = BridgeResponse.getError( BridgeResponse.WrongTypeInMessage )
                
        else:
            reply = BridgeResponse.getError( BridgeResponse.WrongMessageStructure )
        return reply