def importModel( name ):
    __import__( name, globals(), locals(), [], -1 )

#import model modules here
#put model modules in /models directory of the app
importModel( "ClientModel" )
importModel( "PlayerModel" )
importModel( "GameSessionModel" )
importModel( "GameStepModel" )
importModel( "GameModel" )
importModel( "TemplateModel" )