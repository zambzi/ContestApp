'''
Created on Oct 14, 2014

@author: Lukasz Piotrowski

PlayerHelper
Provides useful functions for modification
addition, removal etc. of players, game sessions.

'''
from main.models.PlayerModel import Player
from main.models.ClientModel import Client
import datetime
from twistedBridge import BridgeResponse
from MySQLdb import DatabaseError
from django.core.exceptions import ObjectDoesNotExist
from main.models.GameSessionModel import GameSession
from main.models.GameStepModel import GameStep
from main.helpers.PointCalculationHelper import PointCalculator


def createGameStep( data, key, currentGameSession ):
    '''
        Creates new step for given game session
        @param data : dictionary object associated with game step, consisting of 'key', 'value' and (optional) 'snapshot' fields
        @param key : current Client key
        @param currentGameSession : GameSession in progress
    '''
    if not currentGameSession:
        return BridgeResponse.getError( BridgeResponse.NoSessionInProgress, "" )
    dataKey = data.get("key")
    dataValue = data.get("value")
    if "snapshot" in data:
        dataSnapshot = data.snapshot
    else:
        dataSnapshot = ""
    try:
        gameStep = GameStep( gameSession=currentGameSession, 
                             time=datetime.datetime.now(), 
                             key=dataKey, value=dataValue, 
                             snapshot=dataSnapshot )
        gameStep.save()
        if dataKey == "finished":
            finishGame( currentGameSession )
    except DatabaseError:
        return BridgeResponse.getError(BridgeResponse.CannotCreateGameStep, "")
    return BridgeResponse.getSuccess()
    
    
def finishGame( currentGameSession ):
    '''
        Additional cleanup steps when player finished game (ie. "finished" step has been recieved)
        Sets current GameSession active status on to true/false, depending on number of points
        accumulated ( if there exists GameSession for the same Player with higher point count,
        this game session will become inactive )
        @param currentGameSession : GameSession in progress
        
        @todo perhaps should input point count directly in GameSession for easier debugging?
    '''
#     import ipdb; ipdb.set_trace()
    currentGameSession.active = True
    allSessions = GameSession.objects.filter( player = currentGameSession.player, active = True )
    calc = PointCalculator()
    currentPoints = calc.calculatePoints(currentGameSession)
    for session in allSessions:
        if isCurrentScoreWorse( currentPoints, calc.calculatePoints(session), currentGameSession ):
            currentGameSession.active = False
        else :
            session.active = False
            session.save()
    currentGameSession.save()
        
def isCurrentScoreWorse( currentScore, comparisonScore, currentGameSession ):
    if currentGameSession.player.client.higherScoreWins:
        if float( currentScore ) < float( comparisonScore ) :
            return True
    else :
        if float( currentScore ) > float( comparisonScore ) :
            return True
    return False
    

def createPlayer( name, key, ip, date, client ):
    '''
        adds new player to database
        and returns the reference or returns
        only reference if player with given name 
        exists
        @param name : player name
        @param key : unique Client key
        @param ip : string with peer's ip
        @param date : date of creation
        @param client : instance of client for player
    '''
    try:
        player = Player.objects.get( playerName = name, client = client )
    except ObjectDoesNotExist:
        player = Player( playerName = name, ipAddress = ip, dateCreated = date,  client = client )
        player.save()
    return player

def createSession( playerInstance, date ):
    '''
        adds new game session to database
        @param name : player name
        @param date : date of creation
    '''
    session = GameSession( player = playerInstance, startTime = datetime.datetime.now(), round = playerInstance.client.round )
    session.save();
    return session
    
def startGame( playerName, key, ip ):
    date = datetime.datetime.now()
    try:
        client = Client.objects.get( clientKey = key )
    except ObjectDoesNotExist:
        return BridgeResponse.getError(BridgeResponse.ClientNotFound, "")
    
    try:
        player = createPlayer( playerName, key, ip, date, client )
        currentGameSession = createSession( player, date )
    except DatabaseError:
        return ( BridgeResponse.getError(BridgeResponse.CannotCreateGameSession, ""), None )
    return ( BridgeResponse.getSuccess(), currentGameSession )