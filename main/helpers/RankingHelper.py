'''
Created on Oct 24, 2014

@author: Lukasz Piotrowski

Ranking Helper
Provides functionality for ranking 

'''
from main.models.GameSessionModel import GameSession
from main.models.ClientModel import Client
from main.models.PlayerModel import Player
from django.db.models.query import QuerySet
from main.helpers.PointCalculationHelper import PointCalculator

def getRanking( key, obfuscateName ):
    client = Client.objects.get( clientKey = key ) 
    gameSessions = GameSession.objects.filter( client = client, round = client.round, active = True )
    rankingList = []
    for gameSession in gameSessions:
        if obfuscateName:
            name = obfuscateName( gameSession.player.playerName )
        else:
            name = gameSession.player.playerName
        points = getRankingPoints(gameSession)
        if points <= 0 : continue
        rankingList.append( {
                                "name": name,
                                "points": points,
                                "auxData": "",
                                "postfix": gameSession.player.client.game.rankingPostfix
                             } )
    rankingList = sortRankingList(client, rankingList)
    return rankingList

def sortRankingList( client, rankingList ):
    return sorted(rankingList, key=lambda result: float(result["points"]), reverse=client.orderRankingDesc)

def getRankingPoints( gameSession ):
    calc = PointCalculator()
    return calc.calculatePoints(gameSession)
    
def obfuscateName( name ):
    return name + "XXX"