'''
Created on Oct 23, 2014

@author: Lukasz Piotrowski

Point calculation functions

    Calculation models:
    
        Time:
            Points equals time duration between game step events
            (gets only two first events from list where:
               first is 'from' and second is 'to')
            {type:time, data:event_names_list}
            
        EventCount:
            Points equals the amount of game steps of given type
            {type:eventCount, data:event_list}
            
        EventData:
            Points equals the sum of data value in game steps of given type
            {type:eventData, data:event_list}

'''

from main.models.GameSessionModel import GameSession
from main.models.GameStepModel import GameStep

class PointCalculator:
        
    def calculatePoints(self, gameSession):
        '''
            returns score for given game session
            @param gameSession : GameSession Model instance
            @returns Number of points or -1 if cant calculate
        '''
        if type(gameSession) is GameSession:
            if not self.__isGameSessionValid( gameSession ):
                return -1
            method = self.__getMethod(gameSession.player.client.game.pointCalculationRulesType.type )
            try:
                points = method( gameSession.player.client.game.pointCalculationEventNamesList, gameSession )
                return points
            except TypeError as err:
                print "cannot get points for: " + gameSession.__unicode__() +\
                    ", type: "+gameSession.player.client.game.pointCalculationRulesType.type
                return -1
            except IndexError as err:
                print "No proper steps in game session: " + gameSession.__unicode__()
                return -1
            
    def __isGameSessionValid(self, gameSession):
        '''
            checks for validity of game session based on steps
        '''
        #TODO: add additional fields for game session validity
        steps = GameStep.objects.filter( gameSession = gameSession )
        if len(steps) <= 2:
            return False
        for step in steps:
            if step.key == "finished":
                return True
        return False
            
            
    def __getMethod(self, method):
        if hasattr( self, str(method) + "Model" ):
            return getattr( self,  str(method) + "Model" )
        else: return None
        
    def __clearAndSplitList(self, namesList):
        namesList = namesList.replace(" ", "")
        return namesList.split(",")
        
    def timeModel(self, namesList, gameSession):
        '''
            returns calculated from given game session steps value of
            time between two steps of specified name (only first two
            names matter)
        '''
        list = self.__clearAndSplitList(namesList)
        points = 0
        if len( list ) >= 2 :
            firstStep = GameStep.objects.filter( gameSession = gameSession, key=list[0] )[0]
            lastStep = GameStep.objects.filter( gameSession = gameSession, key=list[1] )[0]
            delta = lastStep.time - firstStep.time
#             import ipdb; ipdb.set_trace()
            precision = "{:."+str(gameSession.client.rankingPrecision)+"f}"
            return str( precision.format( delta.total_seconds() ) )
        else :
            print "Names list is invalid: " + namesList
            return -1
                
    
    def eventCountModel(self, namesList, gameSession):
        '''
            returns count of steps for given game session with
            specified names
        '''
        list = self.__clearAndSplitList(namesList)
        points = 0
        if len( list ) > 0 :
            for name in list:
                steps = GameStep.objects.filter( gameSession = gameSession, key=name )
                points += len( steps )
            return points
        else: return -1
    
    def eventDataModel(self, namesList, gameSession):
        '''
            returns sum of values of steps for given game session
            with specified names
        '''
        #TODO: event data model
        return -1