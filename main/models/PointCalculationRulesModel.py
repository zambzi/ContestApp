'''
Created on Oct 23, 2014

@author: Lukasz Piotrowski

Point Calculation Rules Model
Defines type of point calculation model for game

'''

from django.db import models
  
# Create your models here.
class PointCalculationRules(models.Model):
    type = models.CharField(max_length=255, blank=True, default="")
    
    def __unicode__( self ):
        return self.type