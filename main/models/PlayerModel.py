'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Player Model
Stores information about player

'''

from django.db import models
from main.models.ClientModel import Client
  
# Create your models here.
class Player(models.Model):
    playerName = models.CharField(max_length=255)
    dateCreated = models.DateField()
    ipAddress = models.CharField(max_length=16)
    client = models.ForeignKey(Client)
    
    def __unicode__( self ):
        return self.playerName + ", for client: " + Client.__unicode__( self.client )