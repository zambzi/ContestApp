'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Game Session
stores all information about particular session
by given player

'''

from django.db import models
from main.models.PlayerModel import Player
from main.models.ClientModel import Client
  
class GameSession(models.Model):
    player = models.ForeignKey(Player)
    client = models.ForeignKey(Client, null = True)
    startTime = models.DateTimeField(default = "2014-01-01 00:00")
    stopTime = models.DateTimeField(blank = True, null=True)
    timedout = models.BooleanField(default = False)
    points = models.IntegerField(default = 0)
    round = models.IntegerField(default=0)
    active = models.BooleanField(default=False)
        
    def save(self, *args, **kwargs):
        self.client = self.player.client
        super(GameSession, self).save(*args, **kwargs)       
    
    def __unicode__( self ):
        return  "Game Session by Player: "+Player.__unicode__( self.player )+\
            ", Round: "+str(self.round) + ", ID: "+str(self.id)