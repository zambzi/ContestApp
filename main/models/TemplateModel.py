'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Template model
Stores all the info for given template

'''

from django.db import models
  
# Create your models here.
class Template(models.Model):
    templateName = models.CharField(max_length=255)
    
    def __unicode__( self ):
        return self.templateName