'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Client Model
Stores information on particular client:
template id, game id, client name

'''

from django.db import models
from main.models.TemplateModel import Template
from main.models.GameModel import Game
import hashlib
import datetime
  
# Create your models here.
class Client(models.Model):
    template = models.ForeignKey(Template)
    game = models.ForeignKey(Game)
    domain = models.CharField(max_length=50)
    rulesLink = models.CharField(max_length=255, blank=True)
    clientKey = models.CharField(max_length=50, editable=False)
    clientName = models.CharField(max_length=100)
    clientDescription = models.CharField(max_length=255, blank=True)
    clientNotes = models.TextField(blank=True)
    clientRegex = models.CharField(max_length=255, default="")
    clientTitle = models.CharField(max_length=255, default="")
    clientMetatags = models.TextField(default="", blank=True)
    gameStartDay = models.DateField( default="2000-01-01" )
    gameEndDay = models.DateField( default="2100-01-01" )
    gameStartHour = models.TimeField( default="00:00" )
    gameEndHour = models.TimeField( default="00:00" )
    gameAdditionalSettings = models.TextField(default="", blank=True)
    orderRankingDesc = models.BooleanField( default=False );
    higherScoreWins = models.BooleanField( default=False );
    rankingPrecision = models.IntegerField( default=0 );
    round = models.IntegerField(default=0)
      
    def __unicode__( self ):
        return self.clientName
    
    ''' creates new client-unique key on creation '''
    def createKey(self):
        if len( self.clientKey ) <= 1 :
            hashKey = hashlib.md5()
            hashKey.update( self.clientName + self.domain + Template.__unicode__( self.template ) + datetime.datetime.now().strftime("%Y-%m-%d %H:%M") )
            self.clientKey = str(hashKey.hexdigest())
    
    def save(self, force_insert=False, force_update=False, using=None, 
        update_fields=None):
        self.createKey()
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)