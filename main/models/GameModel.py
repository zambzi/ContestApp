'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Game Model
Stores basic information about game (actual entity, not state)

'''

from django.db import models
from main.models.PointCalculationRulesModel import PointCalculationRules
  
# Create your models here.
class Game(models.Model):
    gameName = models.CharField(max_length=255)
    cssDependencies = models.TextField(default="", blank=True)
    pointCalculationRulesType = models.ForeignKey(PointCalculationRules, null=True)
    pointCalculationEventNamesList = models.TextField(default="", blank=True)
    rankingPostfix = models.CharField(max_length=20, default="", blank=True)
    
    def __unicode__( self ):
        return self.gameName