'''
Created on Oct 3, 2014

@author: Lukasz Piotrowski

Game Step
Records events from the game in form
of time-key-value-snapshot sets
key is the name of the event, value is obviously it's
value, snapshot is a collection of aux data (like physics engine
values dumped for verification in Floppy game)
time is point of time when event got registered

'''

from django.db import models
from main.models.GameSessionModel import GameSession
from main.models.PlayerModel import Player
from main.models.ClientModel import Client
  
# Create your models here.
class GameStep(models.Model): 
    gameSession = models.ForeignKey(GameSession)
    time = models.DateTimeField()
    key = models.CharField(max_length=50)
    value = models.TextField(blank=True)
    snapshot = models.TextField(blank=True)
    
    def __unicode__( self ):
        return self.key+", "+\
            GameSession.__unicode__( self.gameSession )