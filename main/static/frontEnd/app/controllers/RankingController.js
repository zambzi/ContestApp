define(["ember", "bridge"], function(ember, bridge){	
	var applicationController = ember.Controller.extend({
		loadingResults : true,
		results : []
	,
		resultsTimer : 2000,
		showNewItems : false,
		stopReload : false,
		
		startController : function(){
			bridge.rankingCallback = this.onResultsResponse.bind(this);
			this.startRefresh();
		}.on('init'),
		
		startRefresh : function(){
			this.stopReload = false;
			this.callForResults();
			this.repeatedCallForResults();
		},
		
		repeatedCallForResults : function(){
			var that = this;
			if( !this.stopReload ){
				setTimeout(function(){
					requestAnimationFrame( that.repeatedCallForResults.bind(that) );
					that.callForResults();
				}, this.resultsTimer );
			}
		},
	
		callForResults : function(){
			bridge.send( bridge.GET_RANKINGS, "", require("config").clientKey );
		},
		
		onResultsResponse : function( message ){
			this.set("loadingResults", false);
			if( "data" in message && message.status == 0 ){
				if( message.data.length != this.results.length ){
					this.set("showNewItems", false);
				}
				this.set("results", message.data );
			}
			var that = this;
			setTimeout( function(){
				that.set("showNewItems", true);
			},10 );
		},
		
	});
	
	return applicationController;
});