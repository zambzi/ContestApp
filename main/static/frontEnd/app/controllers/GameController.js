/**
 * 
 */

define(["ember", "bridge", "game"], function(ember, bridge, game){
	var GameController = ember.Controller.extend({
		finishedLoading : false,
	
		init : function(){
			console.log("game view");
			//temporarily:
			that = this;
			ember.run.schedule('afterRender', this, function() {
				game.start(	that.onGamePreloaded.bind(that), that.onGameStep.bind(that), that.onGameFinish.bind(that) );
			});
		},
		
		onGameStep : function( data ){
			bridge.send( bridge.GAME_STEP, data, require("config").clientKey );
		},
		
		onGameFinish : function( data ){
			bridge.send( bridge.GAME_STEP, data, require("config").clientKey );
			//set flags for GameSession
		},
		
		onGamePreloaded : function() {
			this.set("finishedLoading", true);
		}
	});
	
	return GameController;
});