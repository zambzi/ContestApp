define(["ember", "bridge"], function(ember, bridge){
	var LoginController = ember.Controller.extend({
		//This regex is defined in database, Client table
		clientRegex : require("config").clientRegex,
		isInputValid : false,
		isEmailValidForSending : true,
		
		init : function(){
			console.log("login view.");
		},
		
		actions : {
			/**
			 * action upon clicking the play button
			 */
			startGame : function(){
				if( this.validateName() ){
					this.set("isEmailValidForSending", true );
					this.loginToGame();
				} else {
					this.set("isEmailValidForSending", false );
				}
			}
		},
		
		loginToGame : function(){
			bridge.send( 2, { name: this.playerName }, require("config").clientKey );
			this.transitionToRoute('game');
		},
		
		/**
		 * Observer for text input, with validation
		 */
		onNameInput : function() {
			if( this.validateName() )
				this.set("isEmailValidForSending", true );
		}.observes('playerName'),
	
		/**
		 * validates bound to login input value playerName
		 * against Client-specific regex 
		 */
		validateName : function(){
			if( typeof this.playerName == "undefined" )
				return false;
			else if( this.playerName.length <= 0 )
				return false;
			var regex = new RegExp( this.clientRegex, "i" );
			this.set('isInputValid', regex.test( this.playerName ));
			return this.isInputValid;
		},
		
	});
	
	return LoginController;
});