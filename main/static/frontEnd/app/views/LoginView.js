define([
    "ember",
    "text!../templates/"+require("config").templateName+"/login.html", ],
    function( ember, template ){
	
	var applicationView = ember.View.extend({
		defaultTemplate : ember.Handlebars.compile(template),
	})
	return applicationView;
});