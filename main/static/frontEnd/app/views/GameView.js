define([
    "ember",
    "text!../templates/"+require("config").templateName+"/game.html", ],
    function( ember, template ){
	
	var applicationView = ember.View.extend({
		defaultTemplate : ember.Handlebars.compile(template),
	})
	return applicationView;
});