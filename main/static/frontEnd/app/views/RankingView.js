define([
    "ember",
    "text!../templates/"+require("config").templateName+"/ranking.html", ],
    function( ember, template ){
	
	var applicationView = ember.View.extend({
		defaultTemplate : ember.Handlebars.compile(template),
		
	    didInsertElement: function(){
	    },
	    
	    startRefresh: function() {
			controller = this.get('controller');
			if( controller != null )
				controller.startRefresh();
	    }.on('willInsertElement'),
	    
		willDestroy : function(){
			controller = this.get('controller');
			controller.stopReload = true;
		},
		
	})
	return applicationView;
});