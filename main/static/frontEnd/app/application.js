define([ "views/LandingView",
         "views/LoginView",
         "views/GameView",
         "views/RankingView",
         "views/AboutView",
         "views/RulesView",
         "controllers/LandingController",
         "controllers/LoginController",
         "controllers/GameController",
         "controllers/RankingController",
         "controllers/AboutController",
         "controllers/RulesController",
         "router", "bridge" ], function( LandingView, LoginView, 
        		 GameView, RankingView, AboutView, RulesView,
        		 LandingController, LoginController, GameController,
        		 RankingController, AboutController, RulesController,
        		 router, bridge ){
	
	var App = {
			LandingView : LandingView,
			GameView : GameView,
			RankingView : RankingView,
			LoginView : LoginView,
			AboutView : AboutView,
			RulesView : RulesView,
//			ApplicationController : ApplicationController,
			LandingController : LandingController,
			GameController : GameController,
			RankingController : RankingController,
			LoginController : LoginController,
			AboutController : AboutController,
			RulesController : RulesController,
			Router : router
	};
	App.Router.map( function(){
		this.resource( 'landing', { path: '/' } );
		this.resource( 'login', { path: '/login' } );
		this.resource( 'game', { path: '/game' } );
		this.resource( 'ranking', { path: '/ranking' } );
		this.resource( 'about', { path: '/about' } );
		this.resource( 'rules', { path: '/rules' } );
	} );

	return App;
});