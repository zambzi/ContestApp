/**
 * returns content from metatag of given name
 * @param name : metatag name attribute
 * @returns metatag content attribute value
 */
function getMetaContent( name ){
	value = document.getElementsByName(name)[0].getAttribute("content");
	return value;
}

/**
 * Removes all selected meta tags
 * @param namesArray : array of name attributes of tags to be removed from DOM
 */
function removeMetatags( namesArray ){
	for( name in namesArray ){
		element = document.getElementsByName(namesArray[name])[0];
		if( typeof element == "undefined" ) continue;
		element.parentNode.removeChild( element );
	}
}

define({
	app_name: "contestApp",
	templateName: getMetaContent( "template_name" ),
	clientKey: getMetaContent( "client_key" ), 
	clientRegex: getMetaContent( "client_regex" ),
	gameName : getMetaContent( "game_name" ),
	rulesLink : getMetaContent( "rules_link" ),
	gameCustomSettings : getMetaContent( "game_custom_settings" ),
	gameAssetsFolder : getMetaContent( "static_url" )+'frontEnd/app/templates/'+getMetaContent( "template_name" )+'/assets/game/'+getMetaContent( "game_name" ),
	shim: {
		'ember' : {
			deps: ['handlebars', 'jquery'],
			exports: 'Ember',
		},
		'gracefulWebSocket' :{
			deps: ['jquery'],
		}
	},
	paths: {
		'App': 'application',
		'models' : 'models',
		'views' : 'views',
		'templates' : 'templates',
		'controllers' : 'controllers',
		'gameFolder' : 'games/'+ getMetaContent( "game_name" ),
		'game' : 'games/'+ getMetaContent( "game_name" ) + '/' + getMetaContent( "game_name" ),
		
		'bridge': 'bridge/bridge',
			
		'jquery': '../lib/jquery.min',
		'gracefulWebSocket': '../lib/jquery.gracefulWebSocket',
		'handlebars': '../lib/handlebars.min',
		'ember': '../lib/ember',
		'emberData' : '../lib/ember-data',
			
		'text': '../lib/text',
		'hbs': '../lib/hbs',
		'domReady': '../lib/domReady',
	},
	hbs : {
		disableI18n: true,
		templateExtension: "html",
	},
})

removeMetatags(["game_name", "template_name", "client_key", "client_regex", "game_custom_settings"]);