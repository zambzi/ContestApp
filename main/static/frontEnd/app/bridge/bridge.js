/**
 * Bridge module.
 * Used for websockets communication
 * 
 */

define([ "gracefulWebSocket" ], function( gracefulWebSocket ){
	var bridge = {
		ws : $.gracefulWebSocket("ws://127.0.0.1:8081/ws"),
		
	    CREATE_PLAYER : 1,
	    START_GAME : 2,
	    FINISH_GAME : 3,
	    GAME_STEP : 4,
	    GET_RANKINGS: 5,
	    
	    rankingCallback : null,
		
		/**
		 * Sends message to server via websockets
		 * @param type : int, defines type of the message
		 * @param data : object, additional data required by given message type
		 * @param key : String, current client key
		 */
		send : function( type, data, key ){
			try{
				var message = 	'{"type":"'	+ type +
								'","data":'	+ JSON.stringify(data) +
								',"key":"' + key +
								'"}';
				console.log( message );
				this.ws.send( message );
			} catch ( err ) {
				alert("Server connection closed");
				console.log( err.message() );
				window.location.href = "/";
			}
		},
		
		onOpen : function( event ){
			console.log("sockets opened");
		},
		
		onMessage : function( event, callback ){
			console.log( event.data );
			message = null;
			try{
				message = JSON.parse( event.data );
			} catch( err ) {
				console.log(err.getMessage());
				message = {
						status : 2000
				}
			}
			if( message["status"] != 0 ){
				return event["status"];
			}
			if( typeof this.rankingCallback == "function" ){
				this.rankingCallback( message );
			}
			
		},
		
		onClose : function( event ){
			console.log("serverClosed!");
			alert("Server connection lost!")
		},
		
		onError : function( event ){
			console.log("serverError!");
			alert("Server error!")
		}
	}
	
	bridge.ws.onmessage = bridge.onMessage.bind(bridge);
	bridge.ws.onclose = bridge.onClose;
	bridge.ws.onerror = bridge.onError;
	bridge.ws.onopen = bridge.onOpen;
	
	return bridge;
});