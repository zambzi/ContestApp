/**
 * Dummy test game created for proof of concept
 */

//Game should be wrapped in require module to be found by system

define([ 	"bridge", 
	         "jquery",
	         //put your dependencies here. Any vars and objects are accessible directly :
	         require("config").paths.gameFolder + "/DummySubfolder/DummyAdditionalFile",
         ],
         function( bridge, jQuery, dummyTalks ){
	
	game = {
		
		canvas : null,
		
		start : function( onPreloadFinished ){
			this.setupCanvas();
			var that = this;
			that.beginGame(onPreloadFinished);
		},
	
		stop : function(){
			
		},
		
		restart : function( onPreloadFinished ){
			var that = this;
			that.beginGame(onPreloadFinished);
		},
		
		beginGame : function( onPreloadFinished ){
			this.colorCanvas();
			that = this;
			setTimeout(function(){
				if( typeof onPreloadFinished == "function" ){
					onPreloadFinished();
				}
				Talker.startConversation(that.canvas, Talker.talk);
			},2000);
		},
			
		setupCanvas : function(){
			this.canvas = document.createElement('canvas');
			this.canvas.id     = "DummyGame";
			this.canvas.width  = jQuery("#game_container").css("width").replace(/[^-\d\.]/g, '');
			this.canvas.height = jQuery("#game_container").css("height").replace(/[^-\d\.]/g, '');
			jQuery(this.canvas).appendTo("#game_container");
		},
		
		colorCanvas : function(){
			var ctx=this.canvas.getContext("2d");
			ctx.fillStyle="#000000";
			ctx.fillRect(0,0,1024,768);
		},
			
			
	};
	
	return game;
	
});