
Talker = {
	talk : ["Hello. This is dummy output. Pay no attention to it.",
	        "I know, you're paying attention to it...",
	        "Stop that.",
	        "I said stop.",
	        "You're dead to me..."	],
	
	startConversation: function( canvas, talk ){
		ctx = canvas.getContext("2d");
		ctx.font = "20px Verdana";
		ctx.fillStyle="#fff";
		for( var i=0; i<talk.length; i++ ){
			this.say(i,talk);

		}
	},

	say : function( i, arr ){
		setTimeout( function(){
			ctx.fillText(arr[i],30,50*i + 100);
		},2500 * i);
	}
		
}
