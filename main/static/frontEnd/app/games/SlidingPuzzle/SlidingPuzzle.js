/**
 * Sliding puzzle game
 */

define([ 	"bridge", 
	         "jquery",
	         //put your dependencies here. Any vars and objects are accessible directly :
	         require("config").paths.gameFolder + "/jquery-migrate-1.2.1.min",
	         require("config").paths.gameFolder + "/jquery.jqpuzzle.full",
	         require("config").paths.gameFolder + "/puzzle-actions"
         ],
         function( bridge, jQuery ){
	
	game = {
		//Main settings for puzzle, downloaded from Client in database
		settings : null,
		onFinish : null,
		onStep : null,
		
		/**
		 * Parses downloaded settings string to valid settings object
		 * @todo: exception handling
		 */
		parseSettings : function(){
			this.settings = jQuery.parseJSON( require("config").gameCustomSettings );
		},
		
		/**
		 * returns shuffle
		 */
		getCurrentShuffle : function(){
			shuffle = [];
			jQuery(".jqp-piece").each( function(){
				shuffle.push( jQuery(this).attr("current") );
			} );
			return shuffle;
		},
		
		/**
		 * sends initial GameStep with shuffle data
		 */
		sendStartingStatus : function(){
			data = {
				key : "shuffle",
				value : this.getCurrentShuffle()
			};
			this.onStep( data );
		},
		
		/**
		 * sets callbacks for actions in puzzle settings
		 */
		setupGameActions : function(){
			this.settings.puzzleSettings.success.callback = this.onPuzzleSuccess.bind(this);
			this.settings.puzzleSettings.movesCallback = this.onPuzzleMove.bind(this);
			this.settings.puzzleSettings.readyCallback = this.sendStartingStatus.bind(this);
		},
		
		onPuzzleMove : function( piece ){
			var data = {
				key : "swap",
				value : jQuery(piece).attr("current"),
			};
			this.onStep( data );
		},
		
		onPuzzleSuccess : function(){
			var data = {
				key: "finished",
				value: this.getCurrentShuffle(),
			}
			this.onFinish( data );
		},
		
		/**
		 * starts the game. Accessed from GameController
		 * @param onPreloadFinished : callback after loading is finished
		 */
		start : function( onPreloadFinished, onGameStep, onGameFinish ){
			this.onFinish = onGameFinish;
			this.onStep = onGameStep;
			
			this.parseSettings();
			this.setupGameActions();
			this.createPuzzleImage();
			this.setPuzzle();

			this.beginGame(onPreloadFinished);
		},
		
		/**
		 * appends image for puzzle creation to game container
		 */
		createPuzzleImage : function(){
			template = "<img id=\"puzzle_image\" src=\""+require("config").gameAssetsFolder+"/puzzle.png\" ></img>";
			jQuery(template).appendTo("#game_container");
		},
		
		/**
		 * creates puzzle from created image inside game container
		 */
		setPuzzle : function(){
			this.puzzleObject = jQuery("#puzzle_image").jqPuzzle(this.settings.puzzleSettings, this.settings.puzzleTexts);
		},
	
		stop : function(){
			
		},
		
		restart : function( onPreloadFinished ){
			
		},
		
		/**
		 * local game starting method
		 * @param onPreloadFinished : callback after loading is finisheds
		 */
		beginGame : function( onPreloadFinished ){
			if( typeof onPreloadFinished == "function" ){
				onPreloadFinished();
			}
//			this.sendStartingStatus();
		},
	};
	
	return game;
	
});