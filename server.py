import sys
import os

from twisted.application import internet, service
from twisted.web import server, resource, wsgi, static
from twisted.python import threadpool
from twisted.internet import reactor, protocol
from twisted.python import log
from twisted.python.log import ILogObserver, FileLogObserver
from twisted.python.logfile import DailyLogFile

import twresource
import django
from django.conf import settings

class ThreadPoolService(service.Service):
    def __init__(self, pool):
        self.pool = pool

    def startService(self):
        service.Service.startService(self)
        self.pool.start()

    def stopService(self):
        service.Service.stopService(self)
        self.pool.stop()

# Environment setup for your Django project files:
sys.path.append("ContestApp")
os.environ['DJANGO_SETTINGS_MODULE'] = 'ContestApp.settings'
from django.core.handlers.wsgi import WSGIHandler

# Twisted Application Framework setup:
application = service.Application('ContestApp')


logfile = DailyLogFile("twisted.log", "./logs")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)

# WSGI container for Django, combine it with twisted.web.Resource:
# The MultiService allows to start Django and Twisted server as a daemon.

multi = service.MultiService()
pool = threadpool.ThreadPool()
tps = ThreadPoolService(pool)
tps.setServiceParent(multi)
resource = wsgi.WSGIResource(reactor, tps.pool, WSGIHandler())
root = twresource.Root(resource)

from twistedBridge.bridge import BridgeFactory
from twisted.protocols import basic
from twisted.web.websockets import WebSocketsResource, WebSocketsProtocol, lookupProtocolForFactory

websocketsResource = WebSocketsResource(lookupProtocolForFactory(BridgeFactory()))
# #serve chat protocol on /ws
root.putChild("ws",websocketsResource)

# mediasrc = static.File( os.path.join(os.path.abspath( "." ), "static" ) )
staticsrc = static.File( os.path.join(os.path.abspath( "." ), "main/static" ) )
# root.putChild( "media", mediasrc )
root.putChild( "static", staticsrc )

django.setup()

# Serve it up:
main_site = server.Site(root)
internet.TCPServer(settings.PORT, main_site).setServiceParent(multi)
multi.setServiceParent(application)