'''
Created on Oct 24, 2014

@author: Lukasz Piotrowski
'''
from django.test import TestCase
from main.models.GameSessionModel import GameSession
from main.models.TemplateModel import Template
from main.models.GameModel import Game
from main.models.PointCalculationRulesModel import PointCalculationRules
from main.models.ClientModel import Client
from main.models.PlayerModel import Player
from datetime import datetime
from datetime import timedelta
from main.models.GameStepModel import GameStep
from main.helpers.PointCalculationHelper import PointCalculator
from random import randint
from main.helpers import RankingHelper

class RankingTestCase(TestCase):
    
    def setUp(self):
        self.template = Template( templateName = "default" )
        self.template.save()
        self.pointCalcRules = PointCalculationRules( type = "eventCount" )
        self.pointCalcRules.save()
        self.game = Game( gameName="test", pointCalculationRulesType = self.pointCalcRules,
                          pointCalculationEventNamesList = "point" )
        self.game.save()
        self.client = Client( template = self.template, game = self.game, domain = "localhost",
                               clientName="test", clientTitle="")
        self.client.save()
        
        players = ["steve", "booby", "buggyB"]
        self.players = []
        self.gameSessions = []
        self.gameStepAmount = 0
        
        for player in players:
            player = Player( playerName = player, dateCreated = datetime.now(), ipAddress="", 
                                  client=self.client )
            self.players.append(player)
            player.save()
            gameSession = GameSession( player=player, startTime=datetime.now() )
            self.gameSessions.append( gameSession )
            gameSession.save()
            self.gameStepAmount += 10
            for x in xrange(0, self.gameStepAmount ):
                print x
                step = GameStep( gameSession=gameSession, 
                                       key="point", time=datetime.now(), value="" )
                step.save()
            stepFinish = GameStep( gameSession=gameSession, 
                                       key="finished", time=datetime.now(), value="" )
            stepFinish.save()
        
    def testReturnedRankingValues(self):
        rankings = RankingHelper.getRanking( self.client.clientKey , False )
        print rankings
        players = ["steve", "booby", "buggyB"]
        for x in xrange(0,3):
            self.assertEqual(rankings[x].get("name"), players[x], "Given Ranking name = "+str(rankings[x].get("name")))
            self.assertEqual(rankings[x].get("points"), 10*(x+1), "Given Ranking points = "+str(rankings[x].get("points")))