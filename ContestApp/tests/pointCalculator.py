from django.test import TestCase
from main.models.GameSessionModel import GameSession
from main.models.TemplateModel import Template
from main.models.GameModel import Game
from main.models.PointCalculationRulesModel import PointCalculationRules
from main.models.ClientModel import Client
from main.models.PlayerModel import Player
from datetime import datetime
from datetime import timedelta
from main.models.GameStepModel import GameStep
from main.helpers.PointCalculationHelper import PointCalculator

class PointCounterTimeTestCase(TestCase):
    
    def setUp(self):
        self.template = Template( templateName = "default" )
        self.template.save()
        self.pointCalcRules = PointCalculationRules( type = "time" )
        self.pointCalcRules.save()
        self.game = Game( gameName="test", pointCalculationRulesType = self.pointCalcRules,
                          pointCalculationEventNamesList = "shuffle, finished" )
        self.game.save()
        self.client = Client( template = self.template, game = self.game, domain = "localhost",
                               clientName="test", clientTitle="")
        self.client.save()
        self.player = Player( playerName = "steve", dateCreated = datetime.now(), ipAddress="", 
                              client=self.client )
        self.player.save()
        self.gameSession = GameSession( player=self.player, startTime=datetime.now() )
        self.gameSession.save()
        now = datetime.now()
        self.gameStep1 = GameStep( gameSession=self.gameSession, 
                                   key="shuffle", time=now, value="10" )
        self.gameStep1.save()
        self.gameStep2 = GameStep( gameSession=self.gameSession, 
                                   key="finished", time=now + timedelta( microseconds=1000 ), value="2" )
        self.gameStep2.save()
        self.gameStep3 = GameStep( gameSession=self.gameSession,
                                   key="test", time=datetime.now(), value="3" )
        self.gameStep3.save()   
    
    def testTimerPoints(self):
        calc = PointCalculator()
        points = calc.calculatePoints(self.gameSession)
        self.assertEquals(points, 1000, "points value = " + str(points))
        
        
class PointCounterEventCountTestCase(TestCase):
    
    def setUp(self):
        self.template = Template( templateName = "default" )
        self.template.save()
        self.pointCalcRules = PointCalculationRules( type = "eventCount" )
        self.pointCalcRules.save()
        self.game = Game( gameName="testCount", pointCalculationRulesType = self.pointCalcRules,
                          pointCalculationEventNamesList = "counters1, counters2" )
        self.game.save()
        self.client = Client( template = self.template, game = self.game, domain = "localhost",
                               clientName="testCount", clientTitle="")
        self.client.save()
        self.player = Player( playerName = "steve", dateCreated = datetime.now(), ipAddress="", 
                              client=self.client )
        self.player.save()
        self.gameSession = GameSession( player=self.player, startTime=datetime.now() )
        self.gameSession.save()
        now = datetime.now()
        
        stepsValues = 0
        
        for x in xrange(1, 4):
            stepsValues += x
            print x
            step = GameStep( gameSession=self.gameSession, 
                                   key="counters1", time=now, value=x )
            step.save()
            
        for x in xrange(1, 4):
            stepsValues += x
            print x
            step = GameStep( gameSession=self.gameSession, 
                                   key="counters2", time=now, value=x )
            step.save()
            
        print stepsValues
        
    def testCountPointsWithoutFinishStep(self):
        calc = PointCalculator()
        points = calc.calculatePoints(self.gameSession)
        self.assertEquals(points, -1, "points value = " + str(points))
    
    def testCountPoints(self):
        step = GameStep( gameSession=self.gameSession, 
                       key="finished", time=datetime.now(), value="10" )
        step.save()
        calc = PointCalculator()
        points = calc.calculatePoints(self.gameSession)
        self.assertEquals(points, 6, "points value = " + str(points))