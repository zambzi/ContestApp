Contest App

Back in the day, there was this one annoying app, that we used for at least
four of our customers. It was simple online sliding-puzzle game with scoreboard
for use during 'corporate contests' for our clients. It was a mess - hard to
refactor and redesign for clients, full of bugs etc.

So I thought to myself "what if we create platform for such minigames, with
scoreboard, with client accounts, easy templating, and ability to inject custom
javascript games. And websockets. Because websockets are cool.".

Enter: Contest App prototype. Basically, simplistic approach to Software-as-
Sevice competition minigame platform.

Basic backend works on Django with Twisted as server. All communication is via
websockets, so everything is in real-time. Admins can write an inject their own
games into the framework, allowing clients to choose from any of those.
Additionaly, frontend is using Durandal SPA with requireJS. Templates can be 
added to the system for easy client management (no editing old html files 
between clients).

Sadly - this project was not maintained for a while, since, my company shifted
focus from those games. However - it should still work.

Additionally - this app allows to register all control inputs in games, so that
admins can 'replay' games that are suspected of cheating. Sadly - the replay
feature was never implemented.